import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';

class BuildBottomSheetHeader extends StatelessWidget {
  const BuildBottomSheetHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 3),
      margin: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.4,
        vertical: 10,
      ),
      decoration: BoxDecoration(
        color: MyColors.grey.withOpacity(.5),
        borderRadius: const BorderRadius.all(Radius.circular(20)),
      ),
    );
  }
}
