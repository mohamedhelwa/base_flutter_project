part of 'user_cubit.dart';

abstract class UserState extends Equatable {
  final UserModel model;
  final bool changed;
  const UserState({required this.model, required this.changed});
}

class UserInitial extends UserState {
  UserInitial()
      : super(
            model: UserModel(
              id: "",
              token: "",
              language: "",
              isOtpVerified: false,
              isProfileComplete: false,
              avatar: "",
              email: "",
              firstName: "",
              lastName: "",
              mobileNumber: "",
              redirect: RedirectType.completeData,
              unreadMessages: false,
              unreadNotifications: false,
              activity: null,
              governorate: null,
              area: null,
              newAccountPoints: null,
              completeAccountPoints: null,
              totalPoints: null,
              machineNumber: "",
            ),
            changed: false);
  @override
  List<Object> get props => [model, changed];
}

class UserUpdateState extends UserState {
  const UserUpdateState({required UserModel model, required bool changed})
      : super(model: model, changed: changed);
  @override
  List<Object> get props => [model, changed];
}
