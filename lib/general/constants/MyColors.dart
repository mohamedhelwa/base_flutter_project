import 'dart:ui';

import 'package:flutter/material.dart';

class MyColors {
  static Color primary = const Color(0xff005A88);
  static Color secondary = const Color(0xffFFD504);
  static Color secondaryText = const Color(0xffFFC303);
  static Color headerColor = const Color(0xffBBDBA1);
  static Color bg = const Color(0xffF7F7F7);
  static Color orange = const Color(0xffF5A515);
  // static Color offWhite = const Color(0xffF2F2F2);
  static Color offWhite = const Color(0xffF7F7F7);
  static Color gold = const Color(0xffe4aa69);
  static Color grey = Colors.grey;
  static Color green = const Color(0xff70C737);
  static Color greenSuccess = const Color(0xff009F00);
  static Color red = const Color(0xffE34211);
  static Color greyWhite = Colors.grey.withOpacity(.2);
  static Color black = Colors.black;
  static Color blackOpacity = Colors.black54;
  static Color white = Colors.white;
  static Color notifyColor = Colors.black54;
  static Color blurColor = const Color(0xffCDF8FE);
  static Color notificationBg = const Color(0xffFFF8E2);
  static Color facebookColor = const Color(0xff1877F2);
  static Color googleColor = const Color(0xffEA4335);
  static Color appleColor = const Color(0xff000000);
  static Color twitterColor = const Color(0xff1DA1F2);
  static Color linkedInColor = const Color(0xff0077B5);
  static Color whatsappColor = const Color(0xff25D366);
  static Color instagramColor = const Color(0xffE1306C);
  static Color youtubeColor = const Color(0xffFF0000);
}
