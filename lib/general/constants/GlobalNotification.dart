import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:base_flutter/general/utilities/utils_functions/debug_helper.dart';
import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:path_provider/path_provider.dart';

class GlobalNotification {
  static StreamController<Map<String, dynamic>> _onMessageStreamController =
      StreamController.broadcast();

  late FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin;
  static late BuildContext context;
  static GlobalNotification instance = GlobalNotification._();
  static FirebaseMessaging messaging = FirebaseMessaging.instance;
  GlobalNotification._();

  setupNotification(BuildContext cxt) async {
    context = cxt;
    _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var android = const AndroidInitializationSettings("@drawable/ic_launcher");
    var ios = const DarwinInitializationSettings();
    var initSettings = InitializationSettings(android: android, iOS: ios);
    _flutterLocalNotificationsPlugin.initialize(initSettings);
    NotificationSettings settings =
        await messaging.requestPermission(provisional: true);
    myPrint('User granted permission: ${settings.authorizationStatus}');
    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      messaging.getToken().then((token) {
        if (token != null) {
          // await Utils.setFireBaseToken(token);
          myPrint(
              "_________________ Firebase token NOTIFICATIONS: _________________");
          myPrint(token);
        }
      });
      messaging.setForegroundNotificationPresentationOptions(
          alert: false, badge: false, sound: false);
      // messaging.getInitialMessage().then((message) =>
      //     _handleInitialMessage(cxt, message != null ? message : null));
      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        myPrint("_____________________Message data:${message.data}");
        _showLocalNotification(message);
        _onMessageStreamController.add(message.data);
        if (int.parse(message.data["type"] ?? "0") == -1) {
          // final SettingsData settingsData = SettingsData();
          // settingsData.logOut(context);
        }
      });
      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        myPrint('A new onMessageOpenedApp event was published!');
        flutterNotificationClick(json.encode(message.data));
      });
      FirebaseMessaging.onBackgroundMessage(
          _firebaseMessagingBackgroundHandler);
    }
  }

  static Future<void> _firebaseMessagingBackgroundHandler(
      RemoteMessage message) async {
    myPrint("Handling a background message: ${message.messageId}");
    myPrint("_____________________Message data:${message.notification!.body}");
    // flutterNotificationClick(json.encode(message.data));
  }

  StreamController<Map<String, dynamic>> get notificationSubject {
    return _onMessageStreamController;
  }

  _showLocalNotification(RemoteMessage? message) async {
    if (message == null) return;
    String? path;
    BigPictureStyleInformation? bigPictureStyleInformation;
    if (message.notification?.android?.imageUrl != null) {
      path = await _downloadAndSaveFile("${message.notification?.title}");
      bigPictureStyleInformation = BigPictureStyleInformation(
        FilePathAndroidBitmap(path),
        largeIcon: FilePathAndroidBitmap(path),
        contentTitle: "${message.notification?.title}",
        summaryText: "${message.notification?.body}",
      );
    }

    var android = AndroidNotificationDetails(
      "${DateTime.now()}",
      "DEFAULT",
      priority: Priority.high,
      importance: Importance.max,
      playSound: true,
      largeIcon: path != null ? FilePathAndroidBitmap(path) : null,
      shortcutId: DateTime.now().toIso8601String(),
      styleInformation: bigPictureStyleInformation,
    );
    var ios = DarwinNotificationDetails();
    var _platform = NotificationDetails(android: android, iOS: ios);
    _flutterLocalNotificationsPlugin.show(
        DateTime.now().microsecond,
        "${message.notification?.title}",
        "${message.notification?.body}",
        _platform,
        payload: json.encode(message.data));
  }

  Future<String> _downloadAndSaveFile(String url) async {
    final Directory directory = await getApplicationDocumentsDirectory();
    final String filePath =
        '${directory.path}/${directory.path.split("/").last}';
    await Dio().download(url, filePath);
    return filePath;
  }

  static Future flutterNotificationClick(String? payload) async {
    myPrint("_____________________payload:$payload");
    var _data = json.decode("$payload");

    int _type = int.parse(_data["type"] ?? "0");
    myPrint("_type: $_type");

    // bool _isLoggedIn = await Utils.savedUserDataFound();

    // if (_isLoggedIn) {
    //   if (_type == -1) {
    //     // final SettingsData settingsData = SettingsData();
    //     // settingsData.logOut(context, isAllDevices: true);
    //   } else if (_type == 2) {
    //     if (AutoRouter.of(context).current.name != NewsDetailsRoute.name) {
    //       if (_data["intent_id"] != null && _data["intent_id"] != "") {
    //         AutoRouter.of(context)
    //             .push(NewsDetailsRoute(newsId: _data["intent_id"]));
    //       } else {
    //         AutoRouter.of(context).push(CommunityNewsRoute());
    //       }
    //     }
    //   } else if (_type == 3) {
    //     if (AutoRouter.of(context).current.name !=
    //         CommunityEventDetailsRoute.name) {
    //       if (_data["intent_id"] != null && _data["intent_id"] != "") {
    //         AutoRouter.of(context)
    //             .push(CommunityEventDetailsRoute(eventId: _data["intent_id"]));
    //       } else {
    //         AutoRouter.of(context).push(CommunityEventsRoute());
    //       }
    //     }
    //   } else {
    //     if (AutoRouter.of(context).current.name != NotificationsRoute.name) {
    //       AutoRouter.of(context).push(NotificationsRoute());
    //       myPrint(
    //           "Notification clicked and navigated to NotificationsRoute");
    //     }
    //   }
    // }
  }

  static Future<void> sendNotification({dynamic fcmData}) async {
    /*
      BackEnd Response style: {"intent":"OPEN_UNIT","intent_id":"62922e012f6ab5d4a68b5a6a"}
    */

    const postUrl = 'https://fcm.googleapis.com/fcm/send';
    Dio dio = Dio();

    var token =
        "ctWn5NCCTQipUqTBLyMYpV:APA91bFy7jLIuEAbM_TAmBcgEN7PsqjCY0zlj4Y-SY563g5fOxRHvFukuzyt6C3ZlJt4W9DWSMsYq3lutdsDmivsDkqG7tANc9xZ0bkmolm2QxwnSshgeG2-hdyItwLxeKr3wscSjo9N";

    myPrint("Sent to token: ${fcmData["to"] ?? token}");

    final data = fcmData ??
        {
          "to": token,
          "click_action": "FLUTTER_NOTIFICATION_CLICK",
          "notification": {
            "body": "This is a notification message from ORA",
            "title": "ORA Egypt",
            "sound": "default",
          },
          "data": {
            "click_action": "FLUTTER_NOTIFICATION_CLICK",
            "type": "2",
            "intent_id": "630cbc0138cabec4f5e79d1e",
            "intent": "OPEN_UNIT",
          }

          // type =  1 ==> Any Notification
          // type =  2 ==> News Notification ==> MUST send newsId
          // type =  3 ==> Event Notification ==> MUST send eventId
          // type = -1 ==> Logout Notification (Block User)
        };

    dio.options.headers['Content-Type'] = 'application/json';
    dio.options.headers["Authorization"] =
        'key=AAAAcRcXQS0:APA91bFW2Rx2Nl8UjIOHCFrXSdxyU3lxUq_MHpRRvo9NCV8pvf0cm9MmGS3Mjjzdk_B5W8VXTmzvaxj_qf-XTn3_M4kw7MR_426KY7jKnMp2wV8fnQLsDwBZsGMplz5Ta7NbhN7i_A1l';

    try {
      final response = await dio.post(postUrl, data: data);

      if (response.statusCode == 200) {
        myPrint('Notification sent successfully');
      } else {
        myPrint('Notification sent failed with status: ${response.statusCode}');
      }
    } catch (e) {
      myPrint('exception $e');
    }
  }
}

/*
{
    "removed": false,
    "isPaid": false,
    "unit": ObjectId("62960756b09a6e1fd1720289"),
    "collectionStatus": "installment",
    "customerBank": "بنك قطر الوطنى الاهلى",
    "code": "35499459",
    "status": "pdc",
    "checkDate": ISODate("2022-08-23T00:00:00.000Z"),
    "amount": 12345,
    "createdAt": ISODate("2022-08-21T12:41:24.140Z"),
    "updatedAt": ISODate("2022-08-21T17:41:24.140Z"),
    "__v": 0
}
 */
