// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'drop_down_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DropDownModel _$DropDownModelFromJson(Map<String, dynamic> json) =>
    DropDownModel(
      id: json['_id'] as String,
      name: json['name'] as String,
      checked: json['checked'] as bool? ?? false,
    );

Map<String, dynamic> _$DropDownModelToJson(DropDownModel instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'name': instance.name,
      'checked': instance.checked,
    };
