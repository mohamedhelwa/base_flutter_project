import 'package:base_flutter/general/models/drop_down_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'UserModel.g.dart';

@JsonSerializable()
class UserModel {
  @JsonKey(name: "_id")
  String? id;
  @JsonKey(name: "token")
  String? token;
  @JsonKey(name: "language", defaultValue: "ar")
  String? language;
  @JsonKey(name: "email")
  String? email;
  @JsonKey(name: "mobileNumber")
  String? mobileNumber;
  @JsonKey(name: "firstName")
  String? firstName;
  @JsonKey(name: "lastName")
  String? lastName;
  @JsonKey(name: "avatar")
  String? avatar;
  @JsonKey(name: "POS")
  String? machineNumber;
  @JsonKey(name: "governorate")
  DropDownModel? governorate;
  @JsonKey(name: "area")
  DropDownModel? area;
  @JsonKey(name: "activity")
  DropDownModel? activity;
  @JsonKey(name: "newAccountPoints")
  String? newAccountPoints;
  @JsonKey(name: "completeAccountPoints")
  String? completeAccountPoints;
  @JsonKey(name: "totalPoints")
  String? totalPoints;
  @JsonKey(name: "isOtpVerified", defaultValue: false)
  bool? isOtpVerified;
  @JsonKey(name: "isProfileComplete", defaultValue: false)
  bool? isProfileComplete;
  @JsonKey(name: "unreadNotifications")
  bool? unreadNotifications;
  @JsonKey(name: "unreadMessages")
  bool? unreadMessages;
  @JsonKey(name: "redirect", defaultValue: RedirectType.completeData)
  RedirectType redirect;

  UserModel({
    this.id,
    this.token,
    this.language = "ar",
    this.redirect = RedirectType.completeData,
    this.isOtpVerified,
    this.isProfileComplete,
    this.email,
    this.mobileNumber,
    this.firstName,
    this.lastName,
    this.avatar,
    this.machineNumber,
    this.governorate,
    this.area,
    this.activity,
    this.newAccountPoints,
    this.completeAccountPoints,
    this.totalPoints,
    this.unreadNotifications,
    this.unreadMessages,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}

enum RedirectType {
  @JsonValue("COMPLETE_DATA")
  completeData,
  @JsonValue("HOME")
  home,
}
