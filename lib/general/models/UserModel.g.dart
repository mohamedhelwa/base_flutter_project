// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UserModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      id: json['_id'] as String?,
      token: json['token'] as String?,
      language: json['language'] as String? ?? 'ar',
      redirect: $enumDecodeNullable(_$RedirectTypeEnumMap, json['redirect']) ??
          RedirectType.completeData,
      isOtpVerified: json['isOtpVerified'] as bool? ?? false,
      isProfileComplete: json['isProfileComplete'] as bool? ?? false,
      email: json['email'] as String?,
      mobileNumber: json['mobileNumber'] as String?,
      firstName: json['firstName'] as String?,
      lastName: json['lastName'] as String?,
      avatar: json['avatar'] as String?,
      machineNumber: json['POS'] as String?,
      governorate: json['governorate'] == null
          ? null
          : DropDownModel.fromJson(json['governorate'] as Map<String, dynamic>),
      area: json['area'] == null
          ? null
          : DropDownModel.fromJson(json['area'] as Map<String, dynamic>),
      activity: json['activity'] == null
          ? null
          : DropDownModel.fromJson(json['activity'] as Map<String, dynamic>),
      newAccountPoints: json['newAccountPoints'] as String?,
      completeAccountPoints: json['completeAccountPoints'] as String?,
      totalPoints: json['totalPoints'] as String?,
      unreadNotifications: json['unreadNotifications'] as bool?,
      unreadMessages: json['unreadMessages'] as bool?,
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      '_id': instance.id,
      'token': instance.token,
      'language': instance.language,
      'email': instance.email,
      'mobileNumber': instance.mobileNumber,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'avatar': instance.avatar,
      'POS': instance.machineNumber,
      'governorate': instance.governorate,
      'area': instance.area,
      'activity': instance.activity,
      'newAccountPoints': instance.newAccountPoints,
      'completeAccountPoints': instance.completeAccountPoints,
      'totalPoints': instance.totalPoints,
      'isOtpVerified': instance.isOtpVerified,
      'isProfileComplete': instance.isProfileComplete,
      'unreadNotifications': instance.unreadNotifications,
      'unreadMessages': instance.unreadMessages,
      'redirect': _$RedirectTypeEnumMap[instance.redirect]!,
    };

const _$RedirectTypeEnumMap = {
  RedirectType.completeData: 'COMPLETE_DATA',
  RedirectType.home: 'HOME',
};
