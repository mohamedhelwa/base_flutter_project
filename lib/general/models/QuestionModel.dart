import 'package:json_annotation/json_annotation.dart';

part 'QuestionModel.g.dart';

@JsonSerializable()
class QuestionModel {
  @JsonKey(name: "question")
  String question;
  @JsonKey(name: "answer")
  String answer;
  @JsonKey(name: "expanded", defaultValue: false)
  bool? expanded;

  QuestionModel({
    required this.question,
    required this.answer,
    this.expanded = false,
  });

  factory QuestionModel.fromJson(Map<String, dynamic> json) =>
      _$QuestionModelFromJson(json);

  Map<String, dynamic> toJson() => _$QuestionModelToJson(this);
}
