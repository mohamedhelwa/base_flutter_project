// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i3;
import 'package:base_flutter/general/screens/login/login_imports.dart' as _i2;
import 'package:base_flutter/general/screens/splash/splash_imports.dart' as _i1;
import 'package:flutter/material.dart' as _i4;

class AppRouter extends _i3.RootStackRouter {
  AppRouter([_i4.GlobalKey<_i4.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i3.PageFactory> pagesMap = {
    SplashRoute.name: (routeData) {
      final args = routeData.argsAs<SplashRouteArgs>();
      return _i3.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i1.Splash(
          key: args.key,
          navigatorKey: args.navigatorKey,
        ),
        opaque: true,
      );
    },
    LoginRoute.name: (routeData) {
      return _i3.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i2.Login(),
        opaque: true,
        barrierDismissible: false,
      );
    },
  };

  @override
  List<_i3.RouteConfig> get routes => [
        _i3.RouteConfig(
          SplashRoute.name,
          path: '/',
        ),
        _i3.RouteConfig(
          LoginRoute.name,
          path: '/Login',
        ),
      ];
}

/// generated route for
/// [_i1.Splash]
class SplashRoute extends _i3.PageRouteInfo<SplashRouteArgs> {
  SplashRoute({
    _i4.Key? key,
    required _i4.GlobalKey<_i4.NavigatorState> navigatorKey,
  }) : super(
          SplashRoute.name,
          path: '/',
          args: SplashRouteArgs(
            key: key,
            navigatorKey: navigatorKey,
          ),
        );

  static const String name = 'SplashRoute';
}

class SplashRouteArgs {
  const SplashRouteArgs({
    this.key,
    required this.navigatorKey,
  });

  final _i4.Key? key;

  final _i4.GlobalKey<_i4.NavigatorState> navigatorKey;

  @override
  String toString() {
    return 'SplashRouteArgs{key: $key, navigatorKey: $navigatorKey}';
  }
}

/// generated route for
/// [_i2.Login]
class LoginRoute extends _i3.PageRouteInfo<void> {
  const LoginRoute()
      : super(
          LoginRoute.name,
          path: '/Login',
        );

  static const String name = 'LoginRoute';
}
