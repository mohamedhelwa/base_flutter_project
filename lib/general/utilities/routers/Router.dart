part of 'RouterImports.dart';

@AdaptiveAutoRouter(
  routes: <AutoRoute>[
    //general routes
    AdaptiveRoute(page: Splash, initial: true),
    CustomRoute(page: Login),
    // AdaptiveRoute(page: Register),
  ],
)
class $AppRouter {}
