part of 'UtilsImports.dart';

class Utils {
  static Future<void> manipulateSplashData(BuildContext context) async {
    await initPackageInfo();
    var lang = GlobalState.instance.get("lang");

    if (lang == null) {
      initDio(lang: "en");
      initCustomWidgets(language: "en");
      DioUtils.lang = "en";
      await checkDeviceSafety(context);
    } else {
      initDio(lang: lang);
      initCustomWidgets(language: lang);
      DioUtils.lang = lang;
      await checkDeviceSafety(context);
    }
  }

  static Future<void> checkDeviceSafety(BuildContext context) async {
    if (!await FlutterJailbreakDetection.jailbroken &&
        (await isRealDevice() || kDebugMode)) {
      await checkUserData(context);
    } else {
      showAlertDialog(
        context: context,
        isDismissible: false,
        showCancelButton: false,
        title: tr(context, "applicationCannotRunOnAJailBrokenOrRootedDevice"),
        confirmText: tr(context, "exit"),
        onConfirm: () => exit(0),
      );
    }
  }

  static Future<bool> isRealDevice() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

    if (Platform.isIOS) {
      var iosInfo = await deviceInfo.iosInfo;
      if (iosInfo.isPhysicalDevice) {
        return true;
      }
    } else if (Platform.isAndroid) {
      var androidInfo = await deviceInfo.androidInfo;
      if (androidInfo.isPhysicalDevice) {
        return true;
      }
    }
    return false;
  }

  static Future<String> loadFileFromAssets(String fileName) async {
    return await rootBundle.loadString(fileName);
  }

  static initDio({required String lang}) {
    DioUtils.init(
      baseUrl: ApiNames.baseUrl,
      style: CustomInputTextStyle(lang: lang),
      primary: MyColors.primary,
      authLink: LoginRoute.name,
      language: lang,
      dismissFunc: EasyLoading.dismiss,
      showLoadingFunc: LoadingDialog.showLoadingDialog,
      authClick: () {},
    );
  }

  static initCustomWidgets({required String language}) {
    WidgetUtils.init(
      style: CustomInputTextStyle(lang: language),
      primary: MyColors.primary,
      language: language,
      inputStyle: ({
        String? label,
        String? hint,
        Widget? prefixIcon,
        Widget? suffixIcon,
        Widget? prefixWidget,
        Widget? suffixWidget,
        Color? hintColor,
        Color? fillColor,
        BorderRadius? radius,
        Color? focusBorderColor,
        EdgeInsets? padding,
        Color? enableColor,
        TextStyle? hintTextStyle,
      }) =>
          CustomInputDecoration(
        lang: language,
        labelTxt: label,
        hint: hint,
        hintColor: MyColors.grey,
        focusColor: MyColors.secondary,
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        enableColor: enableColor,
        customFillColor: fillColor,
        padding: padding,
        borderRaduis: 10,
        hintTextStyle: hintTextStyle ??
            TextStyle(
              color: hintColor ?? MyColors.grey,
              fontFamily: WidgetUtils.lang == "ar"
                  ? "FawryPro"
                  : GoogleFonts.roboto().fontFamily,
            ),
      ),
    );
  }

  static Future<void> initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();

    if (info.version != "") {
      GlobalState.instance.set("packageInfo", info);
      myPrint(
          "___________________________ Package Info ____________________________");
      myPrint("App name: ${info.appName}");
      myPrint("Package name: ${info.packageName}");
      myPrint("Version: ${info.version}");
      myPrint("Build number: ${info.buildNumber}");
      myPrint("Build signature: ${info.buildSignature}");
      myPrint(
          "_____________________________________________________________________");
    }
  }

  static Future<void> checkUserData(BuildContext context) async {
    EncryptedSharedPreferences prefs = EncryptedSharedPreferences();

    var strUser = await prefs.getString("user");

    if (strUser != '') {
      UserModel data = UserModel.fromJson(json.decode("$strUser"));

      if (data.token != null && data.token != "") {
        GlobalState.instance.set("token", data.token);
        changeLanguage(data.language!, context);
        setCurrentUserData(data, context);
      } else {
        AutoRouter.of(context).push(const LoginRoute());
      }
    } else {
      AutoRouter.of(context).push(const LoginRoute());
    }
  }

  static Future<bool> manipulateLoginData(
      BuildContext context, dynamic data) async {
    if (data != null) {
      UserModel user = UserModel.fromJson(data);
      await Utils.setDeviceId(user.token ?? "");
      GlobalState.instance.set("token", user.token);
      myPrint(
          "_______________________________ Login _______________________________");
      myPrint("user.language from LOGIN ==> ${user.language}");

      var lang = user.language;

      if (lang != null && lang != "") {
        await Utils.saveUserData(user);
        Utils.setCurrentUserData(user, context);

        if (lang != WidgetUtils.lang) {
          changeLanguage(lang, context);
          await Future.delayed(const Duration(milliseconds: 100), () {
            CustomToast.showSimpleToast(
                msg: tr(context, "languageChangedToYourPreferences"));
          });
        }
      }
      return true;
    }

    return false;
  }

  static void setCurrentUserData(UserModel model, BuildContext context,
      {bool? redirect = true, bool? fromCompleteProfile = false}) async {
    myPrint("CurrentUserData Token ==> ${model.token}");
    myPrint("CurrentUserData Language ==> ${model.language}");
    myPrint("CurrentUserData ==> ${model.toJson()}");
    myPrint("MobileNumber ==> ${model.mobileNumber}");
    myPrint("Redirect To ==> ${model.redirect}");

    context.read<UserCubit>().onUpdateUserData(model);
    updateAuthorizedStatus(context, true);
    await Utils.saveUserData(model);
    if (redirect!) {
      redirectToScreen(context, model,
          fromCompleteProfile: fromCompleteProfile);
    }
  }

  static void redirectToScreen(BuildContext context, UserModel model,
      {bool? fromCompleteProfile = false}) {
    AutoRouter.of(context).push(const LoginRoute());
    // if (model.redirect == RedirectType.home ||
    //     (model.isProfileComplete ?? false)) {
    //   AutoRouter.of(context).push(HomeRoute(
    //       points: fromCompleteProfile! ? model.completeAccountPoints : null));
    // } else if (model.redirect == RedirectType.completeData) {
    //   AutoRouter.of(context)
    //       .push(CompleteProfileRoute(points: model.newAccountPoints));
    // }
  }

  static Future<void> saveUserData(UserModel model) async {
    EncryptedSharedPreferences prefs = EncryptedSharedPreferences();
    await prefs.setString("user", json.encode(model.toJson()));
  }

  static Future<void> setFirstOpen(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("firstOpen", value);
  }

  static Future<bool> getFirstOpen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool("firstOpen") ?? true;
  }

  static Future<void> setOpenedToday(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("openedToday", value);
  }

  static Future<bool> getOpenedToday() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool("openedToday") ?? false;
  }

  static void copyWithCurrentUserData(
      BuildContext context, UserModel updatedUserData,
      {bool? redirect = true, bool? fromCompleteProfile = false}) {
    UserModel currentUserData = Utils.getSavedUser(context: context);

    currentUserData.id = updatedUserData.id ?? currentUserData.id;
    currentUserData.language =
        updatedUserData.language ?? currentUserData.language;
    currentUserData.token = updatedUserData.token ?? currentUserData.token;

    currentUserData.firstName =
        updatedUserData.firstName ?? currentUserData.firstName;
    currentUserData.lastName =
        updatedUserData.lastName ?? currentUserData.lastName;
    currentUserData.mobileNumber =
        updatedUserData.mobileNumber ?? currentUserData.mobileNumber;
    currentUserData.email = updatedUserData.email ?? currentUserData.email;
    currentUserData.avatar = updatedUserData.avatar ?? currentUserData.avatar;

    currentUserData.machineNumber =
        updatedUserData.machineNumber ?? currentUserData.machineNumber;
    currentUserData.activity =
        updatedUserData.activity ?? currentUserData.activity;
    currentUserData.governorate =
        updatedUserData.governorate ?? currentUserData.governorate;
    currentUserData.area = updatedUserData.area ?? currentUserData.area;

    currentUserData.isProfileComplete =
        updatedUserData.isProfileComplete ?? currentUserData.isProfileComplete;
    currentUserData.isOtpVerified =
        updatedUserData.isOtpVerified ?? currentUserData.isOtpVerified;
    currentUserData.unreadMessages =
        updatedUserData.unreadMessages ?? currentUserData.unreadMessages;
    currentUserData.unreadNotifications = updatedUserData.unreadNotifications ??
        currentUserData.unreadNotifications;

    currentUserData.newAccountPoints =
        updatedUserData.newAccountPoints ?? currentUserData.newAccountPoints;

    currentUserData.completeAccountPoints =
        updatedUserData.completeAccountPoints ??
            currentUserData.completeAccountPoints;

    currentUserData.totalPoints =
        updatedUserData.totalPoints ?? currentUserData.totalPoints;

    currentUserData.redirect =
        updatedUserData.redirect ?? currentUserData.redirect;

    Utils.saveUserData(updatedUserData);
    Utils.setCurrentUserData(currentUserData, context,
        redirect: redirect, fromCompleteProfile: true);
  }

  static Future<bool> savedUserDataFound() async {
    EncryptedSharedPreferences prefs = EncryptedSharedPreferences();
    return await prefs.getString("user") != '';
  }

  static void changeLanguage(String lang, BuildContext context) async {
    DioUtils.lang = lang;
    WidgetUtils.lang = lang;
    context.read<LangCubit>().onUpdateLanguage(lang);

    UserModel? user = Utils.getSavedUser(context: context);
    if (user != null) {
      user.language = lang;
      await Utils.saveUserData(user);
    }
    GlobalState.instance.set("lang", lang);

    myPrint("Current User Lang ==> ${user.language}");
  }

  // static Future<String> uploadFile({
  //   required BuildContext context,
  //   required File file,
  //   String? folderName,
  // }) async {
  //   String uploaderUrl = await GeneralRepository(context)
  //       .getUploaderUrl(file: file, folderName: folderName);
  //
  //   String uploadedFileUrl =
  //       await GeneralRepository(context).uploadFileToUrl(file, uploaderUrl);
  //
  //   myPrint("Uploaded File Url ==> $uploadedFileUrl");
  //   return uploadedFileUrl;
  // }

  static UserModel getSavedUser({required BuildContext context}) {
    return context.read<UserCubit>().state.model;
  }

  static bool isAuthorized({required BuildContext context}) {
    return context.read<AuthCubit>().state.authorized;
  }

  static void updateAuthorizedStatus(BuildContext context, bool authorized) {
    context.read<AuthCubit>().onUpdateAuth(authorized);
  }

  static Future<String?> getDeviceId() async {
    EncryptedSharedPreferences prefs = EncryptedSharedPreferences();
    return await prefs.getString("deviceId");
  }

  static Future<void> setDeviceId(String token) async {
    EncryptedSharedPreferences prefs = EncryptedSharedPreferences();
    await prefs.setString("deviceId", token);
  }

  static Future<void> setServiceCodesFileName(String fileName) async {
    EncryptedSharedPreferences prefs = EncryptedSharedPreferences();
    await prefs.setString("serviceCodes", fileName);
  }

  static Future<String> getServiceCodesFileName() async {
    EncryptedSharedPreferences prefs = EncryptedSharedPreferences();
    String fileName = await prefs.getString("serviceCodes");

    if (fileName == "" || fileName.isEmpty) {
      return "ni-0-v0.0.0.json";
    } else {
      return fileName;
    }
  }

  static void clearSavedData() async {
    EncryptedSharedPreferences prefs = EncryptedSharedPreferences();
    await prefs.clear();
    GlobalState.instance.set("token", null);
  }

  static String getCurrentUserId({required BuildContext context}) {
    var provider = context.watch<UserCubit>().state.model;
    return provider.id ?? "";
  }

  static void copyToClipboard({
    required BuildContext context,
    required String text,
    // required GlobalKey<ScaffoldState> scaffold,
  }) {
    if (text.trim().isEmpty) {
      CustomToast.showToastNotification(tr(context, "thereIsNoTextToCopy"));
      return;
    } else {
      Clipboard.setData(ClipboardData(text: text)).then((value) {
        CustomToast.showToastNotification(
          tr(context, "linkCopiedSuccessfully"),
        );
      });
    }
  }

  static void onControllerTextChange(
      String value, GenericBloc<bool> visibilityCubit) {
    if (value.trim().isNotEmpty) {
      visibilityCubit.onUpdateData(true);
    } else {
      Future.delayed(const Duration(milliseconds: 300),
          () => visibilityCubit.onUpdateData(false));
    }
  }

  static String capitalizeFirstChar(String text) {
    return "${text.substring(0, 1)}${text.substring(1).toLowerCase()}";
  }

  static String formatNumber(dynamic number) {
    var formatter = NumberFormat("#,###");

    if (number != null && number != "") {
      if (number is String) {
        return formatter.format(int.parse(number));
      } else {
        return formatter.format(number);
      }
    } else {
      return "null";
    }
  }

  static String formatDateTime(String dateTime) {
    var formatter = DateFormat("dd MMM yyyy");
    return formatter.format(DateTime.parse(dateTime));
  }

  static String convertDigitsToLatin(String s) {
    var sb = StringBuffer();
    for (int i = 0; i < s.length; i++) {
      switch (s[i]) {
        //Arabic digits
        case '\u0660':
          sb.write('0');
          break;
        case '\u0661':
          sb.write('1');
          break;
        case '\u0662':
          sb.write('2');
          break;
        case '\u0663':
          sb.write('3');
          break;
        case '\u0664':
          sb.write('4');
          break;
        case '\u0665':
          sb.write('5');
          break;
        case '\u0666':
          sb.write('6');
          break;
        case '\u0667':
          sb.write('7');
          break;
        case '\u0668':
          sb.write('8');
          break;
        case '\u0669':
          sb.write('9');
          break;
        default:
          sb.write(s[i]);
          break;
      }
    }
    return sb.toString();
  }

  static void showAlertDialog({
    required context,
    String? title,
    String? contentText,
    Widget? customTitle,
    Widget? customContent,
    List<Widget>? customActions,
    String? confirmText,
    String? cancelText,
    Function()? onConfirm,
    Function()? onCancel,
    bool? isScrollable,
    bool? isDismissible,
    bool? showCancelButton,
    bool? autoDismiss,
    double? borderRadius,
    double? titleFontSize,
    double? contentFontSize,
    Color? titleTextColor,
    Color? contentTextColor,
    Color? confirmButtonColor,
    Color? confirmButtonTextColor,
    Alignment? contentAlignment,
  }) {
    showDialog(
      context: context,
      barrierDismissible: isDismissible ?? true,
      builder: (context) => CustomAlertDialog(
        title: title,
        contentText: contentText,
        customTitle: customTitle,
        customContent: customContent,
        customActions: customActions,
        confirmText: confirmText,
        cancelText: cancelText,
        onConfirm: onConfirm,
        onCancel: onCancel,
        borderRadius: borderRadius,
        titleFontSize: titleFontSize,
        contentFontSize: contentFontSize,
        titleTextColor: titleTextColor,
        contentTextColor: contentTextColor,
        confirmButtonColor: confirmButtonColor,
        confirmButtonTextColor: confirmButtonTextColor,
        contentAlignment: contentAlignment,
        isScrollable: isScrollable,
        isDismissible: isDismissible,
        showCancelButton: showCancelButton,
      ),
    );
    if (autoDismiss ?? false) {
      Future.delayed(const Duration(milliseconds: 1500), () {
        Navigator.of(context).pop();
      });
    }
  }

  static Future<bool> handleBackPress(BuildContext context) async {
    showAlertDialog(
      context: context,
      title: tr(context, "areYouSureYouWantToExit"),
      confirmText: tr(context, "exit"),
      onConfirm: () => SystemNavigator.pop(),
    );
    return Future.value(true);
  }

  static void showComingSoon(BuildContext context) {
    showAlertDialog(
      context: context,
      title: tr(context, "stayTuned"),
      contentText: tr(context, "thisFeatureWillBeAvailableSoon"),
      confirmText: tr(context, "dismiss"),
      showCancelButton: false,
      onConfirm: () => Navigator.of(context).pop(),
    );
  }

  static void showAuthDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: tr(context, "youAreNotLoggedIn"),
      contentText: tr(context, "loginToContinue"),
      confirmText: tr(context, "login"),
      cancelText: tr(context, "dismiss"),
      onConfirm: () =>
          AutoRouter.of(context).popUntilRouteWithName(LoginRoute.name),
    );
  }

  static Future<void> showCupertinoStatusAlert({
    required BuildContext context,
    String? statusText,
    bool? isSuccess,
    bool? barrierDismissible,
    bool? autoPop,
    Duration? autoPopDuration,
    bool? showDismissButton,
    String? dismissButtonText,
    bool? useAnimatedIcons,
    Function()? onDismiss,
  }) async {
    showCupertinoDialog(
      context: context,
      barrierDismissible:
          (autoPop ?? false) ? false : barrierDismissible ?? true,
      builder: (context) => CupertinoAlertDialog(
        title: Container(
          margin: const EdgeInsets.only(bottom: 10),
          child: useAnimatedIcons ?? true
              ? isSuccess ?? true
                  ? Lottie.asset(Res.success,
                      width: 100, height: 100, repeat: false)
                  : Lottie.asset(Res.failure,
                      width: 100, height: 100, repeat: false)
              : Icon(
                  isSuccess ?? true
                      ? CupertinoIcons.check_mark_circled
                      : CupertinoIcons.xmark_circle,
                  color: isSuccess ?? true ? Colors.green : Colors.red,
                  size: 70,
                ),
        ),
        content: MyText(
          title: statusText ??
              (isSuccess ?? true
                  ? tr(context, "success")
                  : tr(context, "error")),
          color: MyColors.black,
          size: 12,
          alien: TextAlign.center,
        ),
        actions: showDismissButton ?? false
            ? [
                CupertinoDialogAction(
                  onPressed: onDismiss ?? () => Navigator.of(context).pop(),
                  child: Text(dismissButtonText ?? tr(context, "dismiss")),
                )
              ]
            : [],
      ),
    );
    if (autoPop ?? false) {
      await Future.delayed(
          autoPopDuration ?? const Duration(milliseconds: 1000), () {
        Navigator.of(context).pop();
      });
    }
  }

  static Future<File?> getImage() async {
    final ImagePicker picker = ImagePicker();
    XFile? image = await picker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 50,
      maxHeight: 800,
      maxWidth: 800,
    );
    if (image != null) {
      File imageFile = File(image.path);
      return imageFile;
    }
    return null;
  }

  static Future<List<File>> getImages() async {
    final ImagePicker picker = ImagePicker();
    final List<XFile> result = await picker.pickMultiImage(
      imageQuality: 50,
      maxHeight: 800,
      maxWidth: 800,
    );
    if (result.isNotEmpty) {
      List<File> files = result.map((e) => File(e.path)).toList();
      return files;
    } else {
      return [];
    }
  }

  static Future<File?> getVideo() async {
    final ImagePicker picker = ImagePicker();
    final XFile? video = await picker.pickVideo(source: ImageSource.gallery);
    if (video != null) {
      File imageFile = File(video.path);
      return imageFile;
    }
    return null;
  }

  static void launchURL({required String url}) async {
    if (!url.toString().startsWith("https")) {
      url = "https://$url";
    }
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      CustomToast.showToastNotification("من فضلك تآكد من الرابط");
    }
  }

  static void launchWhatsApp(phone) async {
    String message = 'مرحبا بك';
    if (phone.startsWith("00966")) {
      phone = phone.substring(5);
    }
    var whatsAppUrl = "whatsapp://send?phone=+966$phone&text=$message";
    myPrint(whatsAppUrl);
    if (await canLaunch(whatsAppUrl)) {
      await launch(whatsAppUrl);
    } else {
      throw 'حدث خطأ ما';
    }
  }

  static void launchYoutube({required String url}) async {
    if (Platform.isIOS) {
      if (await canLaunch(url)) {
        await launch(url, forceSafariVC: false);
      } else {
        if (await canLaunch(url)) {
          await launch(url);
        } else {
          throw 'Could not launch $url';
        }
      }
    } else {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }
  }

  static void callPhone({phone}) async {
    await launch("tel:$phone");
  }

  static void sendMail(mail) async {
    await launch("mailto:$mail");
  }

  static void shareApp(url) {
    LoadingDialog.showLoadingDialog();
    Share.share(url).whenComplete(() {
      EasyLoading.dismiss();
    });
  }

  static void navigateToMapWithDirection({
    required BuildContext context,
    required String lat,
    required String lng,
    String? destinationText,
  }) async {
    if (lat == "0") return;
    try {
      final coordinates = Coords(double.parse(lat), double.parse(lng));
      final availableMaps = await MapLauncher.installedMaps;

      showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        builder: (BuildContext context) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Wrap(
                children: [
                  const BuildBottomSheetHeader(),
                  for (var map in availableMaps)
                    ListTile(
                      onTap: () => map.showMarker(
                        coords: coordinates,
                        title: destinationText ?? tr(context, "destination"),
                      ),
                      title: Text(map.mapName),
                      leading: SvgPicture.asset(
                        map.icon,
                        height: 30.0,
                        width: 30.0,
                      ),
                    ),
                ],
              ),
            ),
          );
        },
      );
    } catch (e) {
      CustomToast.showSimpleToast(msg: "$e");
    }
  }
}
