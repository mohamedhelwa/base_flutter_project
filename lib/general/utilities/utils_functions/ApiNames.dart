class ApiNames {
  static const String baseUrl = "https://stagingapi.fawrymerchants.com/v1/";

  static const String loginWithGoogle = "auth/google-sign-in";
  static const String loginWithFacebook = "auth/facebook-sign-in";
  static const String loginWithApple = "auth/apple-sign-in";
}
