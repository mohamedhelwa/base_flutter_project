import 'package:base_flutter/res.dart';
import 'package:custom_widgets/custom_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class LoadingDialog {
  static showLoadingDialog() {
    EasyLoading.show(
      maskType: EasyLoadingMaskType.black,
      dismissOnTap: false,
      status: WidgetUtils.lang == "ar" ? "جار التحميل..." : "Loading...",
      indicator: SizedBox(
        width: 70,
        height: 70,
        child: Image.asset(Res.placeholder),
      ),
    );
  }

  static showLoadingView({Color? color}) {
    return Center(
      child: SizedBox(
        width: 70,
        height: 70,
        child: Image.asset(Res.placeholder),
      ),
    );
  }
  // static showLoadingDialog() {
  //   EasyLoading.show(
  //       maskType: EasyLoadingMaskType.black,
  //       dismissOnTap: false,
  //       indicator: SpinKitCubeGrid(
  //         size: 40.0,
  //         itemBuilder: (context, index) {
  //           return Container(
  //             height: 10,
  //             width: 10,
  //             margin: EdgeInsets.all(1),
  //             decoration: BoxDecoration(
  //                 color: MyColors.primary, shape: BoxShape.circle),
  //           );
  //         },
  //       ),
  //       status: "loading");
  // }
  //
  // static showLoadingView({Color? color}) {
  //   return Center(
  //     child: SpinKitCubeGrid(
  //       color: color?? MyColors.primary,
  //       size: 40.0,
  //     ),
  //   );
  // }
}
