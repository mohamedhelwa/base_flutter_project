/*
import 'package:hive/hive.dart';

class HiveService {
  static Future<void> openBox(String boxName) async {
    await Hive.openBox(boxName);
  }

  static Future<void> closeBox(String boxName) async {
    await Hive.box(boxName).close();
  }

  static Future<void> deleteBox(String boxName) async {
    await Hive.deleteBoxFromDisk(boxName);
  }

  static Future<void> deleteAllBoxes() async {
    await Hive.deleteFromDisk();
  }

  static Future<void> addData(String boxName, dynamic data) async {
    await Hive.box(boxName).add(data);
  }

  static Future<void> putData(String boxName, dynamic data) async {
    await Hive.box(boxName).put(data.id, data);
  }

  static Future<void> deleteData(String boxName, dynamic data) async {
    await Hive.box(boxName).delete(data.id);
  }

  static Future<void> deleteAllData(String boxName) async {
    await Hive.box(boxName).clear();
  }

  static List<dynamic> getAllData(String boxName) {
    return Hive.box(boxName).values.toList();
  }

  static dynamic getData(String boxName, int id) {
    return Hive.box(boxName).get(id);
  }
}
*/

/*

dynamic readFromCache(String key) {
  return Hive.box("app_memory").get(key);
}

Future<void> writeInCache(String key, dynamic value) async {
  await Hive.box("app_memory").put(key, value);
}

Future<void> clearMemory() async {
  await Hive.box("app_memory").clear();
}

String? get readToken => readFromCache("token");

bool get userNeedToLogin => readFromCache("needToLogin") ?? true;

Future<void> setNoNeedToLogin() async =>
    await writeInCache("needToLogin", false);

Future<void> writeToken(String userToken) async {
  await writeInCache("token", userToken);
}

 */
