part of 'MainDataImports.dart';

class MainData {
  static ThemeData defaultThem = ThemeData(
    focusColor: MyColors.primary,
    primaryColor: MyColors.primary,
    colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.grey)
        .copyWith(secondary: MyColors.primary),
    fontFamily: "FawryPro",
    textTheme: const TextTheme(titleMedium: TextStyle(fontFamily: "FawryPro")),
  );

  static List<BlocProvider> providers(BuildContext context) => [
        BlocProvider<AuthCubit>(create: (BuildContext context) => AuthCubit()),
        BlocProvider<UserCubit>(create: (BuildContext context) => UserCubit()),
      ];
}
