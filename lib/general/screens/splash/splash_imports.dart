import 'package:base_flutter/general/constants/GlobalNotification.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/utils_functions/UtilsImports.dart';
import 'package:base_flutter/general/utilities/utils_functions/debug_helper.dart';
import 'package:base_flutter/general/widgets/header_logo.dart';
import 'package:base_flutter/res.dart';
import 'package:custom_widgets/custom_widgets.dart';
import 'package:custom_widgets/widgets/animation_container.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';

part 'splash.dart';
