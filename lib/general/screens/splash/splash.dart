part of 'splash_imports.dart';

class Splash extends StatefulWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  const Splash({Key? key, required this.navigatorKey}) : super(key: key);

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    _checkingData();
    super.initState();
  }

  _handleInitialFCM() async {
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    if (initialMessage != null) {
      myPrint("Splash initialMessage: ${initialMessage.data}");

      int _type = int.parse(initialMessage.data["type"] ?? "0");
      myPrint("_type: $_type");

      // bool _isLoggedIn = await Utils.savedUserDataFound();

      // if (_isLoggedIn) {
      //   if (_type == -1) {
      //     final SettingsData settingsData = SettingsData();
      //     settingsData.logOut(context, isAllDevices: true);
      //   } else if (_type == 2) {
      //     if (AutoRouter.of(context).current.name != NewsDetailsRoute.name) {
      //       if (initialMessage.data["intent_id"] != null &&
      //           initialMessage.data["intent_id"] != "") {
      //         AutoRouter.of(context).push(
      //             NewsDetailsRoute(newsId: initialMessage.data["intent_id"]));
      //       } else {
      //         AutoRouter.of(context).push(CommunityNewsRoute());
      //       }
      //     }
      //   } else if (_type == 3) {
      //     if (AutoRouter.of(context).current.name !=
      //         CommunityEventDetailsRoute.name) {
      //       if (initialMessage.data["intent_id"] != null &&
      //           initialMessage.data["intent_id"] != "") {
      //         AutoRouter.of(context).push(CommunityEventDetailsRoute(
      //             eventId: initialMessage.data["intent_id"]));
      //       } else {
      //         AutoRouter.of(context).push(CommunityEventsRoute());
      //       }
      //     }
      //   } else {
      //     if (AutoRouter.of(context).current.name != NotificationsRoute.name) {
      //       AutoRouter.of(context).push(NotificationsRoute());
      //       myPrint(
      //           "Notification clicked and navigated to NotificationsRoute from SplashScreen");
      //     }
      //   }
      // }
    }
  }

  _checkingData() async {
    if (!kIsWeb) {
      GlobalNotification.instance.setupNotification(context);
    }

    await Future.delayed(const Duration(milliseconds: 1500), () {
      Utils.manipulateSplashData(context);
    });

    await Future.delayed(const Duration(seconds: 1), () {
      _handleInitialFCM();
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        alignment: Alignment.bottomCenter,
        color: MyColors.white,
        child: Center(
          child: AnimationContainer(
            index: 0,
            vertical: true,
            duration: const Duration(milliseconds: 1500),
            distance: screenHeight * 0.3,
            child: Hero(
              tag: Res.placeholder,
              child: HeaderLogo(width: screenWidth * 0.5),
            ),
          ),
        ),
      ),
    );
  }
}
