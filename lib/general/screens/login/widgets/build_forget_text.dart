part of 'login_widgets_imports.dart';

class BuildForgetText extends StatelessWidget {
  const BuildForgetText({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: InkWell(
        onTap: () {},
        child: MyText(
          title: tr(context, "forgetPassword"),
          size: 13,
          decoration: TextDecoration.underline,
          color: MyColors.primary,
          alien: TextAlign.center,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
