part of 'login_widgets_imports.dart';

class BuildAppNameText extends StatelessWidget {
  const BuildAppNameText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyText(
      title: tr(context, "fawryMerchants"),
      size: 30,
      color: MyColors.primary,
      fontWeight: FontWeight.bold,
    );
  }
}
