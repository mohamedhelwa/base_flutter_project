part of 'login_widgets_imports.dart';

class BuildOrText extends StatelessWidget {
  const BuildOrText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: Divider(
            color: MyColors.grey.withOpacity(.5),
            thickness: 1.5,
            indent: 5,
            endIndent: 5,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
          child: MyText(
            title: tr(context, "or"),
            size: 12,
            alien: TextAlign.center,
            color: MyColors.blackOpacity,
          ),
        ),
        Expanded(
          child: Divider(
            color: MyColors.grey.withOpacity(.5),
            thickness: 1.5,
            indent: 5,
            endIndent: 5,
          ),
        ),
      ],
    );
  }
}
