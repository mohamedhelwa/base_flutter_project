part of 'login_widgets_imports.dart';

class BuildVisitorLoginText extends StatelessWidget {
  const BuildVisitorLoginText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BuildSocialLoginButton(
      onTap: () {},
      title: tr(context, "visitor"),
      textColor: MyColors.black,
      fontSize: WidgetUtils.lang == "ar" ? 16 : 14,
      margin: const EdgeInsets.symmetric(vertical: 25),
    );
  }
}
