part of 'login_widgets_imports.dart';

class BuildSocialLoginList extends StatelessWidget {
  final LoginData loginData;
  const BuildSocialLoginList({Key? key, required this.loginData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15).copyWith(
        top: MediaQuery.of(context).size.height * .12,
      ),
      child: Column(
        children: [
          BuildBoldTitle(
            title: tr(context, "login"),
            padding: const EdgeInsets.only(bottom: 30),
            color: MyColors.primary,
            size: 20,
          ),
          BuildSocialLoginButton(
            iconUrl: Res.facebook,
            onTap: () => loginData.loginWithFacebook(context),
            title: tr(context, "loginWithFacebook"),
            color: MyColors.facebookColor,
          ),
          BuildSocialLoginButton(
            iconUrl: Res.google,
            onTap: () => loginData.loginWithGoogle(context),
            title: tr(context, "loginWithGoogle"),
            color: MyColors.googleColor,
          ),
          if (Platform.isIOS)
            BuildSocialLoginButton(
              iconUrl: Res.apple,
              onTap: () => loginData.loginWithApple(context),
              title: tr(context, "loginWithApple"),
              color: MyColors.appleColor,
            ),
        ],
      ),
    );
  }
}
