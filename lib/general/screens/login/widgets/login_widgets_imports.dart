import 'dart:io';

import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/screens/login/login_imports.dart';
import 'package:base_flutter/general/widgets/build_bold_title.dart';
import 'package:base_flutter/general/widgets/my_text.dart';
import 'package:base_flutter/res.dart';
import 'package:custom_widgets/custom_widgets.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

part 'build_app_name_text.dart';
part 'build_forget_text.dart';
part 'build_form_inputs.dart';
part 'build_header_area.dart';
part 'build_login_button.dart';
part 'build_or_text.dart';
part 'build_social_login_button.dart';
part 'build_social_login_list.dart';
part 'build_visitor_login.dart';
