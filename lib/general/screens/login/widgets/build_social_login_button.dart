part of 'login_widgets_imports.dart';

class BuildSocialLoginButton extends StatelessWidget {
  final String title;
  final Function() onTap;
  final String? iconUrl;
  final Color? color;
  final Color? textColor;
  final double? fontSize;
  final EdgeInsets? margin;

  const BuildSocialLoginButton({
    Key? key,
    required this.title,
    required this.onTap,
    this.margin,
    this.iconUrl,
    this.color,
    this.textColor,
    this.fontSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultButton(
      fontFamily: WidgetUtils.lang == "ar"
          ? "FawryPro"
          : GoogleFonts.roboto().fontFamily,
      color: color ?? MyColors.white,
      borderColor: color ?? MyColors.secondary.withOpacity(0.5),
      height: 60,
      margin: margin ?? const EdgeInsets.only(bottom: 15),
      onTap: onTap,
      child: Row(
        children: [
          if (iconUrl != null)
            Expanded(
              child: Image.asset(
                iconUrl!,
                width: 27,
                height: 27,
                color: MyColors.white,
              ),
            ),
          if (iconUrl != null) const SizedBox(width: 10),
          Expanded(
            flex: iconUrl != null ? 4 : 1,
            child: MyText(
              title: title,
              size: fontSize ?? 13,
              color: textColor ?? MyColors.white,
              fontWeight: FontWeight.bold,
              alien: iconUrl == null ? TextAlign.center : TextAlign.start,
            ),
          ),
        ],
      ),
    );
  }
}
