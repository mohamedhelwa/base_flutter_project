part of 'login_widgets_imports.dart';

class BuildFormInputs extends StatelessWidget {
  final LoginData loginData;

  const BuildFormInputs({Key? key, required this.loginData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      key: loginData.formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // MyText(
            //   title: tr(context, "enterPhoneOrMail"),
            //   size: 12,
            //   color: MyColors.black,
            //   fontWeight: FontWeight.bold,
            // ),
            // GenericTextField(
            //   fieldTypes: FieldTypes.normal,
            //   hint: tr(context, "enterPhoneOrMail"),
            //   controller: loginData.queryController,
            //   margin: const EdgeInsets.symmetric(vertical: 10),
            //   action: TextInputAction.next,
            //   type: TextInputType.text,
            //   fillColor: MyColors.grey.withOpacity(.05),
            //   validate: (value) => value!.validateEmpty(context),
            //   textColor: MyColors.primary,
            // ),
            // MyText(
            //   title: tr(context, "password"),
            //   size: 12,
            //   color: MyColors.black,
            //   fontWeight: FontWeight.bold,
            // ),
            // BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            //   bloc: loginData.passwordVisibilityCubit,
            //   builder: (context, state) {
            //     return GenericTextField(
            //       fieldTypes:
            //           state.data ? FieldTypes.normal : FieldTypes.password,
            //       hint: "********",
            //       controller: loginData.passwordController,
            //       margin: const EdgeInsets.symmetric(vertical: 10)
            //           .copyWith(bottom: 0),
            //       validate: (value) => value!.validatePassword(context),
            //       type: TextInputType.text,
            //       action: TextInputAction.done,
            //       fillColor: MyColors.grey.withOpacity(.05),
            //       onSubmit: () => loginData.userLogin(context),
            //       textColor: MyColors.primary,
            //       suffixIcon: GestureDetector(
            //         onTap: () => loginData.passwordVisibilityCubit
            //             .onUpdateData(!state.data),
            //         child: Icon(
            //           state.data ? Icons.visibility_off : Icons.visibility,
            //           color: MyColors.primary,
            //         ),
            //       ),
            //     );
            //   },
            // ),
          ],
        ),
      ),
    );
  }
}
