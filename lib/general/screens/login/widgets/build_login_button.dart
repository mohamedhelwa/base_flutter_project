part of 'login_widgets_imports.dart';

class BuildLoginButton extends StatelessWidget {
  final LoginData loginData;

  const BuildLoginButton({required this.loginData});

  @override
  Widget build(BuildContext context) {
    return LoadingButton(
      fontFamily: WidgetUtils.lang == "ar"
          ? "FawryPro"
          : GoogleFonts.roboto().fontFamily,
      btnKey: loginData.btnKey,
      title: tr(context, "login"),
      // onTap: () => loginData.userLogin(context),
      onTap: () {},
      color: MyColors.secondary,
      textColor: MyColors.black,
      fontWeight: FontWeight.bold,
      fontSize: WidgetUtils.lang == "ar" ? 18 : 16,
      margin: EdgeInsets.zero.copyWith(bottom: 10),
    );
  }
}
