part of 'login_widgets_imports.dart';

class BuildHeaderArea extends StatelessWidget {
  const BuildHeaderArea({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return Image.asset(Res.placeholder, width: screenWidth * 0.5);
  }
}
