part of 'login_imports.dart';

class LoginData {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<CustomButtonState> btnKey = GlobalKey<CustomButtonState>();
  final GenericBloc<bool> passwordVisibilityCubit = GenericBloc(false);

  final TextEditingController queryController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  // final GoogleSignIn googleSignIn = GoogleSignIn();
  //
  // GoogleSignInAccount? googleSignInAccountUser;
  //
  // GoogleSignInAccount get user => googleSignInAccountUser!;
  //
  // final fb = FacebookLogin();

  Future loginWithGoogle(BuildContext context) async {
    // try {
    //   final googleUser = await googleSignIn.signIn();
    //
    //   if (googleUser != null) {
    //     googleSignInAccountUser = googleUser;
    //
    //     final googleAuth = await googleUser.authentication;
    //
    //     myPrint("access token => ${googleAuth.accessToken}");
    //     myPrint("id token => ${googleAuth.idToken}");
    //
    //     await showLoginSuccessAlert(context, tr(context, "google"));
    //     await completeLoginWithGoogle(context, googleAuth.idToken!,
    //         googleSignInAccountUser!.id, googleSignInAccountUser!.email);
    //   } else {
    //     myPrint("########### GoogleUser null!");
    //     await showLoginFailureAlert(context, tr(context, "google"));
    //     return;
    //   }
    // } catch (e) {
    //   myPrint("ERROR in login with google ==> ${e.toString()}");
    // }
  }

  Future<void> completeLoginWithGoogle(BuildContext context, String googleToken,
      String googleId, String email) async {
    LoadingDialog.showLoadingDialog();
    var result = await GeneralRepository(context)
        .loginWithGoogle(email, googleToken, googleId);

    if (result) {
      // Utils.fetchFireBaseTokenService(context,
      //     messageScreen: "LOGIN-WITH-GOOGLE");
      myPrint(
          "___________________ Login with Google success! ______________________");
    }

    EasyLoading.dismiss();
    // await FirebaseAnalytics.instance.logLogin(loginMethod: 'Google');
  }

  Future loginWithFacebook(BuildContext context) async {
    // aeoyhqukkm_1671626813@tfbnw.net
    // asd@Asd123
    //
    // m_yyzwwxo_m@tfbnw.net
    // M@123456

    // try {
    //   final res = await fb.logIn(permissions: [
    //     FacebookPermission.publicProfile,
    //     FacebookPermission.email,
    //   ]);
    //
    //   switch (res.status) {
    //     case FacebookLoginStatus.success:
    //       final FacebookAccessToken? accessToken = res.accessToken;
    //
    //       myPrint('#### Access token ==> ${accessToken!.token}');
    //
    //       final profile = await fb.getUserProfile();
    //       final imageUrl = await fb.getProfileImageUrl(width: 300, height: 300);
    //       final email = await fb.getUserEmail();
    //
    //       myPrint('#### Profile Name ==> ${profile!.name}');
    //       myPrint('#### User ID ==> ${profile.userId}');
    //       myPrint('#### Profile image ==> $imageUrl');
    //
    //       if (email != null) {
    //         myPrint('#### Email ==> $email');
    //         await showLoginSuccessAlert(context, tr(context, "facebook"));
    //         await completeLoginWithFacebook(
    //             context, accessToken.token, profile.userId, email);
    //       }
    //
    //       break;
    //     case FacebookLoginStatus.cancel:
    //       await showLoginFailureAlert(context, tr(context, "facebook"));
    //       break;
    //     case FacebookLoginStatus.error:
    //       await showLoginFailureAlert(context, tr(context, "facebook"));
    //       myPrint('#### Error while log in: ${res.error}');
    //       break;
    //   }
    // } catch (e) {
    //   myPrint("ERROR in login with facebook ==> ${e.toString()}");
    // }
  }

  Future<void> completeLoginWithFacebook(BuildContext context,
      String facebookToken, String facebookId, String email) async {
    LoadingDialog.showLoadingDialog();
    var result = await GeneralRepository(context)
        .loginWithFacebook(email, facebookToken, facebookId);

    if (result) {
      // Utils.fetchFireBaseTokenService(context,
      //     messageScreen: "LOGIN-WITH-FACEBOOK");
      myPrint(
          "__________________ Login with Facebook success! _____________________");
    }
    EasyLoading.dismiss();
    // await FirebaseAnalytics.instance.logLogin(loginMethod: 'Facebook');
  }

  void loginWithApple(BuildContext context) async {
    Utils.showComingSoon(context);
    // try {
    //   if (await SignInWithApple.isAvailable()) {
    //     final credential = await SignInWithApple.getAppleIDCredential(
    //       scopes: [
    //         AppleIDAuthorizationScopes.email,
    //         AppleIDAuthorizationScopes.fullName,
    //       ],
    //     );
    //
    //     myPrint("#### Email ==> ${credential.email}");
    //     myPrint("#### Identity Token ==> ${credential.identityToken}");
    //     myPrint("#### User Identifier ==> ${credential.userIdentifier}");
    //     myPrint("#### Authorization Code ==> ${credential.authorizationCode}");
    //     myPrint("#### Credential State ==> ${credential.state}");
    //
    //     myPrint("#### Given Name ==> ${credential.givenName}");
    //     myPrint("#### Family Name ==> ${credential.familyName}");
    //
    //     if (credential.identityToken != null) {
    //       await showLoginSuccessAlert(context, "Apple");
    //       await completeLoginWithApple(
    //         context,
    //         "${credential.email}",
    //         "${credential.identityToken}",
    //         "${credential.userIdentifier}",
    //         credential.givenName,
    //         credential.familyName,
    //       );
    //     } else {
    //       await showLoginFailureAlert(context, "Apple");
    //     }
    //   } else {
    //     myPrint("#### Apple Sign In is not available for your device!");
    //     await showLoginFailureAlert(
    //         context, tr(context, "appleSignInIsNotSupported"),
    //         notSupportedMessage: true);
    //   }
    // } catch (e) {
    //   myPrint("ERROR in Sign in with Apple ==> ${e.toString()}");
    //   await showLoginFailureAlert(context, "Apple");
    // }
  }

  Future<void> completeLoginWithApple(
      BuildContext context,
      String email,
      String userIdentifier,
      String identityToken,
      String? firstName,
      String? lastName) async {
    LoadingDialog.showLoadingDialog();
    var result = await GeneralRepository(context).loginWithApple(
        email, identityToken, userIdentifier, firstName, lastName);

    if (result) {
      // Utils.fetchFireBaseTokenService(context,
      //     messageScreen: "LOGIN-WITH-APPLE");
      myPrint(
          "___________________ Login with Apple success! _______________________");
    }
    EasyLoading.dismiss();
    // await FirebaseAnalytics.instance.logLogin(loginMethod: 'Apple');
  }

  Future<void> showLoginSuccessAlert(
      BuildContext context, String loginMethod) async {
    await Utils.showCupertinoStatusAlert(
      context: context,
      statusText: "${tr(context, "successfullyLoggedInWith")} $loginMethod.",
      autoPop: true,
    );
  }

  Future<void> showLoginFailureAlert(BuildContext context, String loginMethod,
      {bool? notSupportedMessage}) async {
    await Utils.showCupertinoStatusAlert(
      context: context,
      statusText: notSupportedMessage ?? false
          ? loginMethod
          : "${tr(context, "failedToLoginWith")} $loginMethod.",
      autoPop: notSupportedMessage ?? false ? false : true,
      isSuccess: false,
      showDismissButton: notSupportedMessage,
    );
  }

  // void userLogin(BuildContext context) async {
  //   FocusScope.of(context).requestFocus(FocusNode());
  //   if (formKey.currentState!.validate()) {
  //     String query = checkEmailOrPhone(queryController.text, context);
  //
  //     if (query != "") {
  //       myPrint("query: $query");
  //
  //       try {
  //         btnKey.currentState!.animateForward();
  //         await GeneralRepository(context)
  //             .userLogin(query, passwordController.text);
  //         btnKey.currentState!.animateReverse();
  //       } catch (e) {
  //         myPrint("Error in Login: $e");
  //         btnKey.currentState!.animateReverse();
  //       }
  //     }
  //   }
  // }

  // String checkEmailOrPhone(String value, BuildContext context) {
  //   String valueEn = Utils.convertDigitsToLatin(value);
  //
  //   if (int.tryParse(valueEn) != null) {
  //     return validateEgyptianNumber(valueEn, context);
  //   } else {
  //     return validateEmail(valueEn, context);
  //   }
  // }
  //
  // String validateEgyptianNumber(String phone, BuildContext context) {
  //   if (phone.length == 11) {
  //     if (phone.startsWith('010') ||
  //         phone.startsWith('011') ||
  //         phone.startsWith('012') ||
  //         phone.startsWith('015')) {
  //       // myPrint("Is Valid Egyptian Number ==> $phoneEn");
  //       return phone;
  //     }
  //   }
  //   CustomToast.showSimpleToast(msg: tr(context, "phoneValidation"));
  //   return "";
  // }
  //
  // String validateEmail(String email, BuildContext context) {
  //   if (RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email)) {
  //     // myPrint("Is Valid Email ==> $email");
  //     return email;
  //   }
  //   CustomToast.showSimpleToast(msg: tr(context, "mailValidation"));
  //   return "";
  // }
}
