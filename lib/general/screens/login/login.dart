part of 'login_imports.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final LoginData loginData = LoginData();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Utils.handleBackPress(context),
      child: AuthScaffold(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 70)
                .copyWith(bottom: MediaQuery.of(context).size.height * .2),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const BuildHeaderArea(),
                BuildSocialLoginList(loginData: loginData),
                const BuildOrText(),
                const BuildVisitorLoginText(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
