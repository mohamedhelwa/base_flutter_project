import 'package:base_flutter/general/models/UserModel.dart';
import 'package:base_flutter/general/utilities/utils_functions/ApiNames.dart';
import 'package:base_flutter/general/utilities/utils_functions/UtilsImports.dart';
import 'package:dio_helper/http/generic_http.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

part 'GeneralHttpMethods.dart';
part 'GeneralRepository.dart';
