part of 'GeneralRepoImports.dart';

class GeneralRepository {
  late BuildContext _context;
  late GeneralHttpMethods _generalHttpMethods;
  GeneralRepository(BuildContext context) {
    _context = context;
    _generalHttpMethods = GeneralHttpMethods(_context);
  }

  Future<bool> loginWithGoogle(
          String email, String googleToken, String googleId) =>
      _generalHttpMethods.loginWithGoogle(email, googleToken, googleId);

  Future<bool> loginWithFacebook(
          String email, String facebookToken, String facebookId) =>
      _generalHttpMethods.loginWithFacebook(email, facebookToken, facebookId);

  Future<bool> loginWithApple(String email, String identityToken,
          String userIdentifier, String? firstName, String? lastName) =>
      _generalHttpMethods.loginWithApple(
          email, identityToken, userIdentifier, firstName, lastName);
}
