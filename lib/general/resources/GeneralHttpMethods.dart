part of 'GeneralRepoImports.dart';

class GeneralHttpMethods {
  final BuildContext context;

  FirebaseMessaging messaging = FirebaseMessaging.instance;

  GeneralHttpMethods(this.context);

  Future<bool> loginWithGoogle(
      String email, String googleToken, String googleId) async {
    Map<String, dynamic> body = {
      "email": email,
      "googleToken": googleToken,
      "googleId": googleId,
    };

    var data = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.loginWithGoogle,
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      returnDataFun: (data) => data,
      toJsonFunc: (json) => UserModel.fromJson(json),
      showLoader: true,
    );

    return Utils.manipulateLoginData(context, data);
  }

  Future<bool> loginWithFacebook(
      String email, String facebookToken, String facebookId) async {
    Map<String, dynamic> body = {
      "email": email,
      "facebookToken": facebookToken,
      "facebookId": facebookId,
    };

    var data = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.loginWithFacebook,
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      returnDataFun: (data) => data,
      toJsonFunc: (json) => UserModel.fromJson(json),
      showLoader: true,
    );

    return Utils.manipulateLoginData(context, data);
  }

  Future<bool> loginWithApple(String email, String identityToken,
      String userIdentifier, String? firstName, String? lastName) async {
    Map<String, dynamic> body = {
      "email": email,
      "appleToken": identityToken,
      "appleId": userIdentifier,
      "firstName": firstName,
      "lastName": lastName,
    };

    var data = await GenericHttp<dynamic>(context).callApi(
      name: ApiNames.loginWithApple,
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      returnDataFun: (data) => data,
      toJsonFunc: (json) => UserModel.fromJson(json),
      showLoader: true,
    );

    return Utils.manipulateLoginData(context, data);
  }
}
