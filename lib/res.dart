/// Generated by AssetsRefGenerator - DO NOT MODIFY BY HAND
class Res {
  static const String apple = "assets/images/apple.png";
  static const String ar = "assets/lang/ar.json";
  static const String en = "assets/lang/en.json";
  static const String facebook = "assets/images/facebook.png";
  static const String failure = "assets/anim/failure.json";
  static const String google = "assets/images/google.png";
  static const String placeholder = "assets/images/placeholder.png";
  static const String success = "assets/anim/success.json";
}
